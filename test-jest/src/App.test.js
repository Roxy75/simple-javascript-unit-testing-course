import { render, fireEvent, screen } from "@testing-library/react";

// Import the component to be tested
import App from "./App";

// Test to ensure that the input field, and submit button are rendered
test("renders input field and submit button", () => {
	// Render the App component
	render(<App />);

	// Get the input field and submit button using screen queries
	const input = screen.getByLabelText("Enter your firstname:");
	const submitButton = screen.getByText("Submit");

	// Assert that the input field and submit button are in the document
	expect(input).toBeInTheDocument();
	expect(submitButton).toBeInTheDocument();
});

// Test to ensure that the first name can be entered and displayed
test("allows entering and displaying the first name", () => {
	// Render the App component
	render(<App />);

	// Get the input field and submit button using screen queries
	const input = screen.getByLabelText("Enter your firstname:");
	const submitButton = screen.getByText("Submit");

	// Simulate typing "John" into the input field and clicking the submit button
	fireEvent.change(input, { target: { value: "John" } });
	fireEvent.click(submitButton);

	// Get the output text displaying the entered first name
	const output = screen.getByText("Your firstname is: John");

	// Assert that the output text is in the document
	expect(output).toBeInTheDocument();
});

// Test to ensure that an error message is displayed if a number is entered as the first name
test("displays an error message if a number is entered as firstname", () => {
	// Render the App component
	render(<App />);

	// Get the input field and submit button using screen queries
	const input = screen.getByLabelText("Enter your firstname:");
	const submitButton = screen.getByText("Submit");

	// Simulate typing "123" into the input field and clicking the submit button
	fireEvent.change(input, { target: { value: "123" } });
	fireEvent.click(submitButton);

	// Get the error message displayed when a number is entered as the first name
	const errorMessage = screen.getByText("Numbers are not authorized !");

	// Assert that the error message is in the document
	expect(errorMessage).toBeInTheDocument();
});
