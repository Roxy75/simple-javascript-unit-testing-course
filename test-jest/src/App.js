import "./App.css";
import React, { useState } from "react";

// Define the App component
const App = () => {
	// Initialize state variables for first name, message, and input visibility
	const [firstName, setFirstName] = useState("");
	const [message, setMessage] = useState("");
	const [inputVisible, setInputVisible] = useState(true);

	// Event handler for input change
	const handleChange = (e) => {
		// Log the value entered in the input field
		console.log(e.target.value);
		// Update the first name state variable with the input value
		setFirstName(e.target.value);
		// Reset the message state variable
		setMessage("");
	};

	// Event handler for button click
	const handleClick = (e) => {
		// Prevent the default form submission behavior
		e.preventDefault();
		// Hide the input field and button
		setInputVisible(false);

		// Check if the entered value is a number
		if (!isNaN(firstName)) {
			// If it's a number, display a message and show the input field again
			setInputVisible(true);
			setMessage("Numbers are not authorized !");
		}
	};

	// Render the component
	return (
		<>
			{inputVisible ? (
				<div>
					{/* Form for entering the first name */}
					<h1>Unit testing</h1>
					<label htmlFor="firstNameInput">Enter your firstname:</label>
					<input type="text" id="firstNameInput" onChange={handleChange} />
					<button onClick={handleClick}>Submit</button>
					{message && <p>{message}</p>}
				</div>
			) : (
				// Display the entered first name
				<p>Your firstname is: {firstName}</p>
			)}
		</>
	);
};

// Export the App component
export default App;
