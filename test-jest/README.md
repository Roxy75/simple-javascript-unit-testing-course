# UNIT TEST 💯


## Basic Course on Unit Testing for Beginners
### Introduction to Unit Testing

Unit testing is a programming practice of writing tests to verify the correct operation of individual parts (units) of a program in isolation.

### Why Unit Testing is Important:

1. Code Reliability: Unit tests help detect errors earlier in the development process, leading to more reliable code.
2. Ease of Maintenance: They make code maintenance easier by allowing developers to quickly detect regressions introduced by new changes.
3. Living Documentation: Unit tests also serve as living documentation for the code, describing how each part of the application should work.

### Testing Tools:
To write unit tests, you can use different testing frameworks and libraries depending on the programming language you're using. Here are some commonly used tools:

- JavaScript: Jest, Mocha, Jasmine.
- Python: unittest, pytest.
- Java: JUnit.
- C#: NUnit, MSTest.

### Writing Unit Tests:

1. Test Planning: Before writing tests, identify the most important features and test cases to cover.
2. Writing Tests: Unit tests should be written to be independent of each other and test a single unit of code at a time (function, method, class).
3. Running Tests: Tests should be run regularly to detect errors as soon as they occur.
4. Test Maintenance: Tests should be kept up-to-date based on changes made to the application code.

### Organizing Tests:
There are several ways to organize unit tests in a project:
- Alongside Components: Place tests in the same directory as the components or modules being tested.
- Dedicated Test Folder: Create a separate folder to store all tests for the application.
- _ _ tests _ _ Folder: Use a special folder named _ _ tests _ _ in each directory to store tests.

### Conclusion:
Unit testing is an essential practice to ensure code quality and reliability in software development. By writing effective unit tests and integrating them into your development process, you can reduce errors, ease code maintenance, and ensure the robustness of your applications.

To run your tests, use either "npm test" or "yarn test".



**Example**
Here we will see an example of testing using Jest and React Testing Library.
To perform tests with Jest, you can use the @testing-library/react library to simulate user interaction with our component and verify its behavior.

If you're using Create React App (CRA), Jest is included by default as part of the testing infrastructure provided by CRA. When you create a new project with CRA by running the command npx create-react-app my-project or yarn create react-app my-project, Jest is automatically configured to handle unit tests in your project.


We want that:
- The first test checks that the input field and the submit button are rendered correctly.
- The second test simulates entering a valid first name, then clicking the submit button, and verifies that the first name is displayed correctly.
- The third test simulates entering a number as the first name, then clicking the submit button, and verifies that the error message is displayed correctly.


##### ############################################################################


# TEST UNITAIRE 💯


## Cours de base sur les tests unitaires pour les débutants
### Introduction aux Tests Unitaires

Les tests unitaires sont une pratique de programmation consistant à écrire des tests pour vérifier le bon fonctionnement des différentes parties (unités) d'un programme de manière isolée.

### Pourquoi les Tests Unitaires sont Importants:

1. Fiabilité du Code : Les tests unitaires permettent de détecter les erreurs plus tôt dans le processus de développement, ce qui conduit à un code plus fiable.
2. Facilité de Maintenance : Ils facilitent la maintenance du code en permettant aux développeurs de détecter rapidement les régressions introduites par de nouvelles modifications.
3. Documentation Vivante : Les tests unitaires servent également de documentation vivante pour le code, décrivant comment chaque partie de l'application devrait fonctionner.

### Outils de Test:
Pour écrire des tests unitaires, vous pouvez utiliser différents frameworks et bibliothèques de test selon le langage de programmation que vous utilisez. Voici quelques-uns des outils les plus couramment utilisés :

- JavaScript : Jest, Mocha, Jasmine.
- Python : unittest, pytest.
- Java : JUnit.
- C# : NUnit, MSTest.

### Écrire des Tests Unitaires:

1. Planification des Tests : Avant d'écrire des tests, identifiez les fonctionnalités et les cas de test les plus importants à couvrir.
2. Écriture des Tests : Les tests unitaires doivent être écrits de manière à être indépendants les uns des autres et à tester une seule unité de code à la fois (fonction, méthode, classe).
3. Exécution des Tests : Les tests doivent être exécutés régulièrement pour détecter les erreurs dès qu'elles surviennent.
4. Maintenance des Tests : Les tests doivent être maintenus à jour en fonction des modifications apportées au code de l'application.

### Organisation des Tests
Il existe plusieurs façons d'organiser les tests unitaires dans un projet :
- À côté des Composants : Placez les tests dans le même répertoire que les composants ou les modules à tester.
- Dossier Dédié aux Tests : Créez un dossier séparé pour stocker tous les tests de l'application.
- Dossier _ _ tests _ _ : Utilisez un dossier spécial nommé _ _ tests _ _ dans chaque répertoire pour stocker les tests.

Pour lancer vos tests, faites un "run test" ou "yarn test".



DOCS:
# https://testing-library.com/docs/
# https://github.com/testing-library/jest-dom

**Exemple** 
Ici nous allons voir un exemple de tests à l'aide de Jest et de React Testing Library.
Pour effectuer des tests avec Jest, vous pouvez utiliser la bibliothèque @testing-library/react pour simuler l'interaction de l'utilisateur avec notre composant et vérifier son comportement.

Si vous utilisez Create React App (CRA), Jest est inclus par défaut dans le cadre de l'infrastructure de test fournie par CRA. Lorsque vous créez un nouveau projet avec CRA en exécutant la commande npx create-react-app mon-projet ou yarn create react-app mon-projet, Jest est automatiquement configuré pour gérer les tests unitaires dans votre projet.

Nous souhaitons donc que:
- Le premier test vérifie que le champ d'entrée et le bouton de soumission sont rendus correctement.
- Le deuxième test simule la saisie d'un prénom valide, puis clique sur le bouton de soumission, et vérifie que le prénom est correctement affiché.
- Le troisième test simule la saisie d'un nombre comme prénom, puis clique sur le bouton de soumission, et vérifie que le message d'erreur est correctement affiché.
